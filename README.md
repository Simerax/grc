# GRC - a git clone utility
grc is just a git clone with a small difference - it automates the process of creating a folder structure.

For example `grc https://gitlab.com/Simerax/grc` does the following `git clone https://gitlab.com/Simerax/grc $HOME/code/gitlab.com/Simerax/grc` and then cd's into that directory 
The next time you run `grc https://gitlab.com/Simerax/grc` it will skip the clone (because the directory already exists) and just cd's into it.

That's it.

The only thing that is important for this script is that it only works well if you source it (`. ./grc`) instead of executing it directly `./grc`.  
The reason for this is that running a script puts that script into its own environment/shell and therefore `cd` does not propagate into your current shell.


### How to setup
1. Put the grc script somewhere in your `$PATH`
2. Make an alias to grc in your favorite shell. In ZSH this would look like this `alias grc=". grc"`


First Run:
```
$ ~/ grc https://gitlab.com/Simerax/grc
>> cloning https://gitlab.com/Simerax/grc -> /home/fx/code/gitlab.com/Simerax/grc
>> Klone nach '/home/fx/code/gitlab.com/Simerax/grc'...
>> cd to /home/fx/code/gitlab.com/Simerax/grc
$ ~/code/gitlab.com/Simerax/grc/
```

Second Run:
```
$ ~/ grc https://gitlab.com/Simerax/grc
>> Directory already exists. skipping clone. chdir to /home/fx/code/gitlab.com/Simerax/grc
$ ~/code/gitlab.com/Simerax/grc/
```


